﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace example.Controllers
{
    public class ScoreController : Controller
    {
        // GET: Score
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(int score)
        {
            string level = "";

            if(score <= 100 && score >= 80)
            {
                level = "A";
            }
            else if(score < 80 && score >= 60)
            {
                level = "B";
            }
            else if (score < 60 && score >= 40)
            {
                level = "C";
            }
            else if (score < 40 && score >= 20)
            {
                level = "D";
            }
            else if (score < 20 && score >= 0)
            {
                level = "E";
            }
            else
            {
                level = "請輸入0-100分";
            }

            ViewBag.Score = score;
            ViewBag.level = level;
            return View();
        }
    }
}