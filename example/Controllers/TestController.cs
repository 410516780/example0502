﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace example.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Login()
        {
            TempData["message"] = "Hello";
            return RedirectToAction("Index");
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}