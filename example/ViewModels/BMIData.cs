﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace example.ViewModels
{
    public class BMIData
    {
        [Required(ErrorMessage ="必填欄位")]
        [Range(30, 150, ErrorMessage="請輸入30-150的數值")]
        [Display(Name ="體重")]
        public float Weight { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [Range(50, 250, ErrorMessage = "請輸入50-250的數值")]
        [Display(Name = "身高")]
        public float Height { get; set; }
        public float? BMI { get; set; }
        public string Level { get; set; }
    }
}